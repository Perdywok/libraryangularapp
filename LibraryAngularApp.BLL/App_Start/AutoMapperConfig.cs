﻿using AutoMapper;
using LibraryAngularApp.BLL.Profiles;

namespace LibraryAngularApp.BLL.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BookProfile>();
                cfg.AddProfile<PublicationProfile>();
                cfg.AddProfile<BrochureProfile>();
                cfg.AddProfile<AuthorProfile>();
                cfg.AddProfile<PaperProductProfile>();         
            });
        }
    }
}