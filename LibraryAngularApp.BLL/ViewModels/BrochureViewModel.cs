﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryAngularApp.BLL.ViewModels
{
    public class BrochureViewModel
    {
        public BrochureViewModel()
        {
            Authors = new List<AuthorViewModel>();
        }

        public int BrochureId { get; set; }

        [Display(Name = "Brochure Name")]
        public string BrochureName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public virtual List<AuthorViewModel> Authors { get; set; }
    }
}
