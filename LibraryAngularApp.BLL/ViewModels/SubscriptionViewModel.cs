﻿using LibraryAngularApp.DAL.Entities.Enums;
using System;

namespace LibraryAngularApp.BLL.ViewModels
{
    public class SubscriptionViewModel
    {
        public int SubscriptionId { get; set; }
        public string SubscriptionName { get { return Enum.GetName(typeof(Subscription), SubscriptionId); } }
    }
}

