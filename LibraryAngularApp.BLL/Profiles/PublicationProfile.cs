﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Entities.Enums;
using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.BLL.Profiles
{
    public class PublicationProfile : Profile
    {
        public PublicationProfile()
        {
            CreateMap<Publication, PublicationViewModel>()
            .ForMember(p => p.PublicationId, opt => opt.MapFrom(p => p.PublicationId))
            .ForMember(p => p.PublicationName, opt => opt.MapFrom(p => p.PublicationName))
            .ForMember(p => p.Pages, opt => opt.MapFrom(p => p.Pages))
            .ForMember(p => p.Publisher, opt => opt.MapFrom(p => p.Publisher))
            .ForMember(p => p.PublicationDate, opt => opt.MapFrom(p => p.PublicationDate))
             .ForMember(p => p.Subscription, opt => opt.MapFrom(p => new SubscriptionViewModel
             {
                 SubscriptionId = ((int)p.Subscription)
             }))
             .ForMember(dto => dto.Authors, opt => opt.MapFrom(b => b.PublicationAuthors.Select(a => a.Author).ToList()));
            CreateMap<List<PublicationAuthor>, List<AuthorViewModel>>();

            CreateMap<PublicationViewModel, Publication>()
                .ForMember(p => p.PublicationId, opt => opt.MapFrom(p => p.PublicationId))
                .ForMember(p => p.PublicationName, opt => opt.MapFrom(p => p.PublicationName))
                .ForMember(p => p.Pages, opt => opt.MapFrom(p => p.Pages))
                .ForMember(p => p.Publisher, opt => opt.MapFrom(p => p.Publisher))
                .ForMember(p => p.PublicationDate, opt => opt.MapFrom(p => p.PublicationDate))
                .ForMember(p => p.Subscription, opt => opt.MapFrom(p => p.Subscription.SubscriptionId))
                .ForMember(b => b.PublicationAuthors,
                    opt => opt.ResolveUsing(bvm =>
                    {
                        var result = new List<PublicationAuthor>();
                        foreach (var author in bvm.Authors)
                        {
                            var publication = new PublicationAuthor
                            {
                                AuthorId = author.AuthorId,
                                PublicationId = bvm.PublicationId
                            };
                            result.Add(publication);
                        }
                        return result;
                    }));
        }
    }
}
