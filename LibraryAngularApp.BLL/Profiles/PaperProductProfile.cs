﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.BLL.Profiles
{
    public class PaperProductProfile : Profile
    {
        public PaperProductProfile()
        {
            CreateMap<Book, PaperProductViewModel>()
                .ForMember(b => b.PaperProductId, opt => opt.MapFrom(b => b.BookId))
                .ForMember(b => b.PaperProductName, opt => opt.MapFrom(b => b.BookName))
                .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
                .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
                .ForMember(b => b.Type, opt => opt.MapFrom(b => typeof(Book).Name))
               .ForMember(dto => dto.Authors, opt => opt.MapFrom(b => b.BookAuthors.Select(a => a.Author).ToList()));
            CreateMap<List<BookAuthor>, List<AuthorViewModel>>();

            CreateMap<Brochure, PaperProductViewModel>()
               .ForMember(b => b.PaperProductId, opt => opt.MapFrom(b => b.BrochureId))
               .ForMember(b => b.PaperProductName, opt => opt.MapFrom(b => b.BrochureName))
               .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
               .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
               .ForMember(b => b.Type, opt => opt.MapFrom(b => typeof(Brochure).Name))
               .ForMember(dto => dto.Authors, opt => opt.MapFrom(b => b.BrochureAuthors.Select(a => a.Author).ToList()));
            CreateMap<List<BrochureAuthor>, List<AuthorViewModel>>();

            CreateMap<Publication, PaperProductViewModel>()
               .ForMember(p => p.PaperProductId, opt => opt.MapFrom(p => p.PublicationId))
               .ForMember(p => p.PaperProductName, opt => opt.MapFrom(p => p.PublicationName))
               .ForMember(p => p.Pages, opt => opt.MapFrom(p => p.Pages))
               .ForMember(p => p.Publisher, opt => opt.MapFrom(p => p.Publisher))
               .ForMember(b => b.Type, opt => opt.MapFrom(b => typeof(Publication).Name))
                .ForMember(dto => dto.Authors, opt => opt.MapFrom(b => b.PublicationAuthors.Select(a => a.Author).ToList()));
            CreateMap<List<PublicationAuthor>, List<AuthorViewModel>>();
        }
    }
}
