﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.BLL.Profiles
{
    public class BrochureProfile : Profile
    {
        public BrochureProfile()
        {
            CreateMap<Brochure, BrochureViewModel>()
             .ForMember(b => b.BrochureId, opt => opt.MapFrom(b => b.BrochureId))
             .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
             .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
             .ForMember(b => b.BrochureName, opt => opt.MapFrom(b => b.BrochureName))
            .ForMember(dto => dto.Authors, opt => opt.MapFrom(b => b.BrochureAuthors.Select(a => a.Author).ToList()));
            CreateMap<List<BrochureAuthor>, List<AuthorViewModel>>();

            CreateMap<BrochureViewModel, Brochure>()
             .ForMember(b => b.BrochureId, opt => opt.MapFrom(b => b.BrochureId))
            .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
            .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
            .ForMember(b => b.BrochureName, opt => opt.MapFrom(b => b.BrochureName))
             .ForMember(b => b.BrochureAuthors,
                    opt => opt.ResolveUsing(bvm =>
                    {
                        var result = new List<BrochureAuthor>();
                        foreach (var author in bvm.Authors)
                        {
                            var brochure = new BrochureAuthor
                            {
                                AuthorId = author.AuthorId,
                                BrochureId = bvm.BrochureId
                            };
                            result.Add(brochure);
                        }
                        return result;
                    }));

        }
    }
}
