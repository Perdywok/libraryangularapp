﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.BLL.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookViewModel>()
                .ForMember(b => b.BookId, opt => opt.MapFrom(b => b.BookId))
                .ForMember(b => b.BookName, opt => opt.MapFrom(b => b.BookName))
                .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
                .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
                .ForMember(b => b.Genre, opt => opt.MapFrom(b => new GenreViewModel
                {
                    GenreId = ((int)b.Genre)
                }))
                    .ForMember(dto => dto.Authors, opt => opt.MapFrom(b => b.BookAuthors.Select(a => a.Author).ToList()));
            CreateMap<List<BookAuthor>, List<AuthorViewModel>>();

            CreateMap<BookViewModel, Book>()
                .ForMember(b => b.BookId, opt => opt.MapFrom(b => b.BookId))
                .ForMember(b => b.BookName, opt => opt.MapFrom(b => b.BookName))
                .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
                .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
                .ForMember(b => b.Genre, opt => opt.MapFrom(b => b.Genre.GenreId))
             .ForMember(b => b.BookAuthors,
                    opt => opt.ResolveUsing(bvm =>
                    {
                        var result = new List<BookAuthor>();
                        foreach (var author in bvm.Authors)
                        {
                            var book = new BookAuthor
                            {
                                AuthorId = author.AuthorId,
                                BookId = bvm.BookId
                            };
                            result.Add(book);
                        }
                        return result;
                    }));

        }
    }
}
