﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Entities.Enums;
using LibraryAngularApp.DAL.Entities.JoinTables;
using LibraryAngularApp.DAL.Repos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.BLL.Services
{
    public class PublicationService
    {
        EFUnitOfWork db { get; set; }

        public PublicationService(Library context)
        {
            db = new EFUnitOfWork(context);
        }

        public List<PublicationViewModel> GetAllObjects()
        {
            List<Publication> publications = db.Publications.GetAllPublications();
            List<PublicationViewModel> viewModelPublications = publications.Select(e => Mapper.Map<Publication, PublicationViewModel>(e)).ToList();
            return viewModelPublications;
        }

        public List<AuthorViewModel> GetAuthors()
        {
            List<AuthorViewModel> authors = db.Authors.GetAllAuthors().Select(e => Mapper.Map<Author, AuthorViewModel>(e)).ToList();
            return authors;
        }

        public void UpdateObject(PublicationViewModel viewModel)
        {
            Publication parsedViewModel = Mapper.Map<PublicationViewModel, Publication>(viewModel);
            Publication publication = db.Publications.Find(viewModel.PublicationId);
            publication.PublicationId = parsedViewModel.PublicationId;
            publication.PublicationName = parsedViewModel.PublicationName;
            publication.Subscription = parsedViewModel.Subscription;
            publication.Pages = parsedViewModel.Pages;
            publication.Publisher = parsedViewModel.Publisher;
            publication.PublicationDate = parsedViewModel.PublicationDate;
            UpdateAssociatedObject(publication, parsedViewModel);
        }

        public void UpdateAssociatedObject(Publication publicationToUpdate, Publication viewModelPublication)
        {
            AddUpdatedAuthors(publicationToUpdate, viewModelPublication);
            RemoveUpdatedAuthors(publicationToUpdate, viewModelPublication);
            db.SaveChanges();
        }

        public int CreateObject(PublicationViewModel viewModel)
        {
            Publication viewModelPublication = Mapper.Map<PublicationViewModel, Publication>(viewModel);
            db.Publications.Create(viewModelPublication);
            db.SaveChanges();

            return viewModelPublication.PublicationId;
        }

        public void DeleteObject(PublicationViewModel viewModel)
        {
            Publication publication = db.Publications.FindOne(viewModel.PublicationId);
            if (publication != null)
            {
                db.Books.Delete(publication.PublicationId);
            }
            db.SaveChanges();
        }

        private void AddUpdatedAuthors(Publication publicationToUpdate, Publication viewModelPublication)
        {
            foreach (var authors in viewModelPublication.PublicationAuthors)
            {
                var searchPublicationAuthor = publicationToUpdate.PublicationAuthors.Find(b => b.AuthorId == authors.AuthorId);
                if (searchPublicationAuthor == null)
                {
                    publicationToUpdate.PublicationAuthors.Add(authors);
                }
            }
        }

        private void RemoveUpdatedAuthors(Publication publicationToUpdate, Publication viewModelPublication)
        {
            int countOfAuthors = publicationToUpdate.PublicationAuthors.Count;
            for (int i = 0; i < countOfAuthors; i++)
            {
                PublicationAuthor searchPublicationAuthor = null;
                foreach (var viewModelAuthors in viewModelPublication.PublicationAuthors)
                {
                    if (viewModelAuthors.AuthorId == publicationToUpdate.PublicationAuthors[i].AuthorId)
                    {
                        searchPublicationAuthor = viewModelAuthors;
                    }
                }
                if (searchPublicationAuthor == null)
                {
                    publicationToUpdate.PublicationAuthors.Remove(publicationToUpdate.PublicationAuthors[i]);
                    i--;
                    countOfAuthors--;
                }
            }
        }
        public List<SubscriptionViewModel> GetAllSubscriptions()
        {
            List<SubscriptionViewModel> allSubscriptions = Enum.GetValues(typeof(Subscription)).Cast<Subscription>()
                .Select(v => new SubscriptionViewModel
            {
                SubscriptionId = ((int)v)
            }).ToList();
            return allSubscriptions;
        }

    }
}
