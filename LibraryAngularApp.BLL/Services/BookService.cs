﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Entities.Enums;
using LibraryAngularApp.DAL.Entities.JoinTables;
using LibraryAngularApp.DAL.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
namespace LibraryAngularApp.BLL.Services
{
    public class BookService
    {
        EFUnitOfWork db { get; set; }
        public BookService(Library context)
        {
            db = new EFUnitOfWork(context);
        }

        public List<BookViewModel> GetAllObjects()
        {
            List<Book> books = db.Books.GetAllBooks();
            List<BookViewModel> viewModelbooks = books.Select(e => Mapper.Map<Book, BookViewModel>(e)).ToList();
            return viewModelbooks;
        }

        public List<AuthorViewModel> GetAuthors()
        {
            List<AuthorViewModel> authors = db.Authors.GetAllAuthors().Select(e => Mapper.Map<Author, AuthorViewModel>(e)).ToList();
            return authors;
        }

        public void UpdateObject(BookViewModel viewModel)
        {
            Book parsedBook = Mapper.Map<BookViewModel, Book>(viewModel);
            Book book = db.Books.Find(viewModel.BookId);
            book.BookId = parsedBook.BookId;
            book.BookName = parsedBook.BookName;
            book.Genre = parsedBook.Genre;
            book.Pages = parsedBook.Pages;
            book.Publisher = parsedBook.Publisher;
            UpdateAssociatedObject(book, parsedBook);
        }

        public void UpdateAssociatedObject(Book bookToUpdate, Book viewModelBook)
        {
            AddUpdatedAuthors(bookToUpdate, viewModelBook);
            RemoveUpdatedAuthors(bookToUpdate, viewModelBook);
            db.SaveChanges();
        }

        public int CreateObject(BookViewModel viewModel)
        {
            var parseBook = Mapper.Map<BookViewModel, Book>(viewModel);
            Book viewModelBook = parseBook;
            //при добавлении книги новое айди увеличивается на 1000 один раз на одну сессию(прикол с скл 2012)
            db.Books.Create(viewModelBook);
            db.SaveChanges();

            return viewModelBook.BookId;
        }

        public void DeleteObject(BookViewModel viewModel)
        {
            Book book = db.Books.FindOne(viewModel.BookId);
            if (book != null)
            {
                db.Books.Delete(book.BookId);
            }
            db.SaveChanges();
        }

        public List<GenreViewModel> GetAllGenres()
        {
            List<GenreViewModel> allGenres = Enum.GetValues(typeof(Genre)).Cast<Genre>().Select(v => new GenreViewModel
            {
                GenreId = ((int)v)
            }).ToList();
            return allGenres;
        }

        private void AddUpdatedAuthors(Book bookToUpdate, Book viewModelBook)
        {
            foreach (var authors in viewModelBook.BookAuthors)
            {
                var searchBookAuthor = bookToUpdate.BookAuthors.Find(b => b.AuthorId == authors.AuthorId);
                if (searchBookAuthor == null)
                {
                    bookToUpdate.BookAuthors.Add(authors);
                }
            }
        }

        private void RemoveUpdatedAuthors(Book bookToUpdate, Book viewModelBook)
        {
            int countOfAuthors = bookToUpdate.BookAuthors.Count;
            for (int i = 0; i < countOfAuthors; i++)
            {
                BookAuthor searchBookAuthor = null;
                foreach (var viewModelAuthors in viewModelBook.BookAuthors)
                {
                    if (viewModelAuthors.AuthorId == bookToUpdate.BookAuthors[i].AuthorId)
                    {
                        searchBookAuthor = viewModelAuthors;
                    }
                }
                if (searchBookAuthor == null)
                {
                    bookToUpdate.BookAuthors.Remove(bookToUpdate.BookAuthors[i]);
                    i--;
                    countOfAuthors--;
                }
            }
        }
    }
}
