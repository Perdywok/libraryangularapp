﻿using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL.Entities;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using LibraryAngularApp.DAL.Repos;
using LibraryAngularApp.DAL;
using LibraryAngularApp.DAL.Entities.JoinTables;

namespace LibraryAngularApp.BLL.Services
{
    public class BrochureService
    {
        EFUnitOfWork db { get; set; }
        public BrochureService(Library context)
        {
            db = new EFUnitOfWork(context);
        }

        public List<BrochureViewModel> GetAllObjects()
        {
            List<Brochure> brochures = db.Brochures.GetAllBrochures();
            List<BrochureViewModel> viewModelBrochures = brochures.Select(e => Mapper.Map<Brochure, BrochureViewModel>(e)).ToList();
            return viewModelBrochures;
        }

        public List<AuthorViewModel> GetAuthors()
        {
            List<AuthorViewModel> authors = db.Authors.GetAllAuthors().Select(e => Mapper.Map<Author, AuthorViewModel>(e)).ToList();
            return authors;
        }

        public void UpdateObject(BrochureViewModel viewModel)
        {
            Brochure parsedBrochure = Mapper.Map<BrochureViewModel, Brochure>(viewModel);
            Brochure brochure = db.Brochures.Find(viewModel.BrochureId);
            brochure.BrochureId = parsedBrochure.BrochureId;
            brochure.BrochureName = parsedBrochure.BrochureName;
            brochure.Pages = parsedBrochure.Pages;
            brochure.Publisher = parsedBrochure.Publisher;          
            UpdateAssociatedObject(brochure, parsedBrochure);
        }

        public void UpdateAssociatedObject(Brochure brochureToUpdate, Brochure viewModelBrochure)
        {
            AddUpdatedAuthors(brochureToUpdate, viewModelBrochure);
            RemoveUpdatedAuthors(brochureToUpdate, viewModelBrochure);
            db.SaveChanges();
        }

        public int CreateObject(BrochureViewModel viewModel)
        {
            Brochure viewModelBrochure = Mapper.Map<BrochureViewModel, Brochure>(viewModel);
            db.Brochures.Create(viewModelBrochure);
            db.SaveChanges();

            return viewModelBrochure.BrochureId;
        }

        public void DeleteObject(BrochureViewModel viewModel)
        {
            Brochure brochure = db.Brochures.FindOne(viewModel.BrochureId);
            if (brochure != null)
            {
                db.Brochures.Delete(brochure.BrochureId);
            }
            db.SaveChanges();
        }

        private void AddUpdatedAuthors(Brochure brochureToUpdate, Brochure viewModelBrochure)
        {
            foreach (var authors in viewModelBrochure.BrochureAuthors)
            {
                var searchBrochureAuthor = brochureToUpdate.BrochureAuthors.Find(b => b.AuthorId == authors.AuthorId);
                if (searchBrochureAuthor == null)
                {
                    brochureToUpdate.BrochureAuthors.Add(authors);
                }
            }
        }

        private void RemoveUpdatedAuthors(Brochure brochureToUpdate, Brochure viewModelBrochure)
        {
            int countOfAuthors = brochureToUpdate.BrochureAuthors.Count;
            for (int i = 0; i < countOfAuthors; i++)
            {
                BrochureAuthor searchBrochureAuthor = null;
                foreach (var viewModelAuthors in viewModelBrochure.BrochureAuthors)
                {
                    if (viewModelAuthors.AuthorId == brochureToUpdate.BrochureAuthors[i].AuthorId)
                    {
                        searchBrochureAuthor = viewModelAuthors;
                    }
                }
                if (searchBrochureAuthor == null)
                {
                    brochureToUpdate.BrochureAuthors.Remove(brochureToUpdate.BrochureAuthors[i]);
                    i--;
                    countOfAuthors--;
                }
            }
        }
    }
}
