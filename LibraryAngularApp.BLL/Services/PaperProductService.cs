﻿using AutoMapper;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL;
using LibraryAngularApp.DAL.Entities;
using LibraryAngularApp.DAL.Repos;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.BLL.Services
{
    public class PaperProductService
    {
        EFUnitOfWork db { get; set; }

        public PaperProductService(Library context)
        {
            db = new EFUnitOfWork(context);
        }

        public IEnumerable<PaperProductViewModel> GetAllObjects()
        {

            List<PaperProductViewModel> paperProducts = new List<PaperProductViewModel>();

            var books = db.Books.GetAllBooks().Select(e => Mapper.Map<Book, PaperProductViewModel>(e)).ToList();
            var publications = db.Publications.GetAllPublications().Select(e => Mapper.Map<Publication, PaperProductViewModel>(e)).ToList();
            var brochures = db.Brochures.GetAllBrochures().Select(e => Mapper.Map<Brochure, PaperProductViewModel>(e)).ToList();
           
            paperProducts.AddRange(books);
            paperProducts.AddRange(publications);
            paperProducts.AddRange(brochures);
            return paperProducts;
        }
    }
}
