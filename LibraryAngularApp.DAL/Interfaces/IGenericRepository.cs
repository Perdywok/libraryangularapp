﻿namespace LibraryAngularApp.DAL.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void Create(TEntity item);

        void Delete(int id);   
    }
}
