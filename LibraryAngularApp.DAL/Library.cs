﻿namespace LibraryAngularApp.DAL
{
    using Microsoft.EntityFrameworkCore;
    using LibraryAngularApp.DAL.Entities;
    using LibraryAngularApp.DAL.Entities.Enums;
    using System;
    using System.Linq;
    using LibraryAngularApp.DAL.Entities.JoinTables;

    public class Library : DbContext
    {
        public Library(DbContextOptions<Library> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookAuthor>().HasKey(sc => new { sc.BookId, sc.AuthorId });
            modelBuilder.Entity<BookAuthor>()
          .HasOne(x => x.Book)
          .WithMany(x => x.BookAuthors)
          .HasForeignKey(x => x.BookId);

            modelBuilder.Entity<BookAuthor>()
               .HasOne(x => x.Author)
               .WithMany(x => x.BookAuthors)
               .HasForeignKey(x => x.AuthorId);

            modelBuilder.Entity<BrochureAuthor>().HasKey(sc => new { sc.BrochureId, sc.AuthorId });
            modelBuilder.Entity<PublicationAuthor>().HasKey(sc => new { sc.PublicationId, sc.AuthorId });
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Book> Books { get; set; }

        public DbSet<Brochure> Brochures { get; set; }

        public DbSet<Publication> Publications { get; set; }

        public DbSet<Author> Authors { get; set; }
    }
    public class LibraryDbInitializer
    {
        public static void Initialize(Library db)
        {
            db.Database.EnsureCreated();
            if (!db.Authors.Any())
            {
                Author s1 = new Author { AuthorName = "Pyshkin" };
                Author s2 = new Author { AuthorName = "Leonardo" };
                Author s3 = new Author { AuthorName = "Georgii" };
                Author s4 = new Author { AuthorName = "Loman" };
                db.Authors.Add(s1);
                db.Authors.Add(s2);
                db.Authors.Add(s3);
                db.Authors.Add(s4);
                db.SaveChanges();
            }

            if (!db.Books.Any())
            {

                Book c1 = new Book
                {             
                    BookName = "Операционные системы",
                    Pages = 250,
                    Genre = Genre.Drama
                };
                Book c2 = new Book
                {
                    BookName = "Алгоритмы и структуры данных",
                    Pages = 250,
                    Genre = Genre.Drama,
                };

                db.Books.Add(c1);
                db.Books.Add(c2);

                db.SaveChanges();
            }
            
            if (!db.Brochures.Any())
            {
                Brochure b1 = new Brochure
                {
                    BrochureName = "FirstBrochure",
                    Pages = 42,
                    Publisher = "SomeGuy",
                };

                Brochure b2 = new Brochure
                {
                    BrochureName = "SecondBrochure",
                    Pages = 39,
                    Publisher = "AnotherGuy"
                
                };

                db.Brochures.Add(b1);
                db.Brochures.Add(b2);
                db.SaveChanges();
            }

            if (!db.Publications.Any())
            {
                Publication p1 = new Publication
                {
                    PublicationName = "FirstPublication",
                    Pages = 25,
                    Publisher = "SomeGuy",
                    Subscription = Subscription.NoSubscription,
                    PublicationDate = DateTime.Now
                };

                Publication p2 = new Publication
                {
                    PublicationName = "SecondPublication",
                    Pages = 28,
                    Publisher = "SomeGuy",
                    Subscription = Subscription.Month,
                    PublicationDate = DateTime.Now,
                };

                db.Publications.Add(p1);
                db.Publications.Add(p2);
                db.SaveChanges();
            }
            
        }
    }

}