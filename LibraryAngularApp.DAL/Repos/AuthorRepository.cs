﻿using LibraryAngularApp.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.DAL.Repos
{
    public class AuthorRepository : EFGenericRepository<Author>
    {
        Library db;
        public AuthorRepository(Library context) : base(context)
        {
            db = context;
        }
        public List<Author> GetAllAuthors()
        {
            List<Author> authors = db.Authors.ToList();
            return authors;
        }
    }
}
