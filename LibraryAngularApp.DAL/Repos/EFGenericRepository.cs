﻿using LibraryAngularApp.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LibraryAngularApp.DAL.Repos
{
    public class EFGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        DbContext _context;
        DbSet<TEntity> _dbSet;

        public EFGenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public void Create(TEntity item)
        {
            _dbSet.Add(item);
        }

        public void Delete(int id)
        {
            var searchItem = FindOne(id);
            if (searchItem != null)
            {
                _dbSet.Remove(searchItem);
            }
        }

        public TEntity FindOne(int id)
        {
            var searchItem = _dbSet.Find(id);               
            return searchItem;
        }
    }
}
