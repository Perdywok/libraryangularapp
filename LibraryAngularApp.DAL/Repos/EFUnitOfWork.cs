﻿using System;

namespace LibraryAngularApp.DAL.Repos
{
    public class EFUnitOfWork
    {
        private Library db;
        private BookRepository bookRepository;
        private BrochureRepository brochureRepository;
        private PublicationRepository publicationRepository;
        private AuthorRepository authorRepository;

        public EFUnitOfWork(Library context)
        {
            db = context;
        }
        public BookRepository Books
        {
            get
            {
                if (bookRepository == null)
                    bookRepository = new BookRepository(db);
                return bookRepository;
            }
        }
        public BrochureRepository Brochures
        {
            get
            {
                if (brochureRepository == null)
                    brochureRepository = new BrochureRepository(db);
                return brochureRepository;
            }
        }
        public PublicationRepository Publications
        {
            get
            {
                if (publicationRepository == null)
                    publicationRepository = new PublicationRepository(db);
                return publicationRepository;
            }
        }

        public AuthorRepository Authors
        {
            get
            {
                if (authorRepository == null)
                    authorRepository = new AuthorRepository(db);
                return authorRepository;
            }
        }
        public void SaveChanges()
        {
            db.SaveChanges();
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
