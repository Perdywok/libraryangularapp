﻿using LibraryAngularApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.DAL.Repos
{
    public class PublicationRepository : EFGenericRepository<Publication>
    {
        Library db;
        public PublicationRepository(Library context) : base(context)
        {
            db = context;
        }
        public List<Publication> GetAllPublications()
        {
            List<Publication> publications = db.Publications.Include(b => b.PublicationAuthors).ThenInclude(pa => pa.Author).ToList();
            return publications;
        }
        public Publication Find(int id)
        {
            Publication searchedPublication = db.Publications
              .Include(b => b.PublicationAuthors)
              .ThenInclude(pa => pa.Author)
              .SingleOrDefault(p => p.PublicationId == id);

            return searchedPublication;
        }
    }
}
