﻿using LibraryAngularApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.DAL.Repos
{
    public class BookRepository : EFGenericRepository<Book>
    {
        Library db;

        public BookRepository(Library context) : base(context)
        {
            db = context;
        }
        public List<Book> GetAllBooks()
        {
            List<Book> books = db.Books.Include(b => b.BookAuthors).ThenInclude(x=>x.Author).ToList();
            return books;
        }
        public Book Find(int id)
        {
            Book searchedBook = db.Books
              .Include(b => b.BookAuthors)
              .ThenInclude(b => b.Author)
              .SingleOrDefault(b => b.BookId == id);

            return searchedBook;
        }
    }
}
