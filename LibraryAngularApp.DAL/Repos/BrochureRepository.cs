﻿using LibraryAngularApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAngularApp.DAL.Repos
{
    public class BrochureRepository : EFGenericRepository<Brochure>
    {
        Library db;
        public BrochureRepository(Library context) : base(context)
        {
            db = context;
        }
        public List<Brochure> GetAllBrochures()
        {
            List<Brochure> brochures = db.Brochures.Include(b => b.BrochureAuthors).ThenInclude(ba => ba.Author).ToList();
            return brochures;
        }
        public Brochure Find(int id)
        {
            Brochure searchedBrochure = db.Brochures
              .Include(b => b.BrochureAuthors)
              .ThenInclude(b => b.Author)
              .SingleOrDefault(b => b.BrochureId == id);

            return searchedBrochure;
        }
    }
}
