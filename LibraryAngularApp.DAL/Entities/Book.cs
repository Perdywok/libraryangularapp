﻿using LibraryAngularApp.DAL.Entities.Enums;
using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;

namespace LibraryAngularApp.DAL.Entities
{
    public class Book
    {
        public Book()
        {
            BookAuthors = new List<BookAuthor>();
        }

        public int BookId { get; set; }

        public string BookName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public Genre Genre { get; set; }

        public List<BookAuthor> BookAuthors { get; set; }

    }
}

