﻿using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;

namespace LibraryAngularApp.DAL.Entities
{
    public class Brochure
    {
        public Brochure()
        {
            BrochureAuthors = new List<BrochureAuthor>();
        }
        public int BrochureId { get; set; }

        public string BrochureName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public List<BrochureAuthor> BrochureAuthors { get; set; }
    }
}
