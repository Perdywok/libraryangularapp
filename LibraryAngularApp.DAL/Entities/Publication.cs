﻿using LibraryAngularApp.DAL.Entities.Enums;
using LibraryAngularApp.DAL.Entities.JoinTables;
using System;
using System.Collections.Generic;

namespace LibraryAngularApp.DAL.Entities
{
    public class Publication
    {
        public Publication()
        {
            PublicationAuthors = new List<PublicationAuthor>();
        }
        public int PublicationId { get; set; }

        public string PublicationName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public DateTime PublicationDate { get; set; }

        public Subscription Subscription { get; set; }

        public List<PublicationAuthor> PublicationAuthors { get; set; }
    }
}
