﻿using LibraryAngularApp.DAL.Entities.JoinTables;
using System.Collections.Generic;
namespace LibraryAngularApp.DAL.Entities
{
    public class Author
    {
        public Author()
        {
            BookAuthors = new List<BookAuthor>();
        }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public List<BookAuthor> BookAuthors { get; set; }
    }
}
