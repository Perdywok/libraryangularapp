﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryAngularApp.DAL.Entities.JoinTables
{
    public class BrochureAuthor
    {
        public int BrochureId { get; set; }
        public Brochure Brochure { get; set; }

        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
