﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryAngularApp.DAL.Entities.JoinTables
{
    public class PublicationAuthor
    {
        public int PublicationId { get; set; }
        public Publication Publication { get; set; }

        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
