import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { GridModule } from '@progress/kendo-angular-grid';
import { PopupModule } from '@progress/kendo-angular-popup';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownListModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

import { AppComponent } from './app.component';
import { BookService } from './book/book.service';
import { PopupAnchorDirective } from './popup.anchor-target.directive';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { BookComponent } from './book/book.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CounterComponent } from './counter/counter.component';





import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PublicationComponent } from './publication/publication.component';
import { BrochureComponent } from './brochure/brochure.component';
import { PaperProductComponent } from './paper-product/paper-product.component';
import { BrochureService } from './brochure/brochure.service';
import { PublicationService } from './publication/publication.service';
import { PaperProductService } from './paper-product/paper-product.service';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    PopupAnchorDirective,
    HomeComponent,
    FetchDataComponent,
    BookComponent,
    CounterComponent,
    PublicationComponent,
    BrochureComponent,
    PaperProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    GridModule,
    DropDownsModule,
    PopupModule,
    InputsModule,
    DateInputsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'home', component: HomeComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'book', component: BookComponent },
      { path: 'brochure', component: BrochureComponent },
      { path: 'publication', component: PublicationComponent },
      { path: 'paperProduct', component: PaperProductComponent },
      { path: '**', redirectTo: 'home' }
    ]),
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
     
    BookService,
    BrochureService,
    PublicationService,
    PaperProductService,
    HttpClient,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
