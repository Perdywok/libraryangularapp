import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { BookService } from './book.service';
import { HttpClient } from '@angular/common/http';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Genre } from '../models/genre';
import { Author } from '../models/author';

  const createFormGroup = dataItem => new FormGroup({
  'bookId': new FormControl(dataItem.bookId),
  'bookName': new FormControl(dataItem.bookName, Validators.required),
  'pages': new FormControl(dataItem.pages),
  'publisher': new FormControl(dataItem.publisher),
    'genre': new FormControl(dataItem.genre, Validators.required),
   'authors': new FormControl(dataItem.authors)
});

@Component({
  selector: 'my-app',
  templateUrl: './book.component.html'
})
export class BookComponent implements OnInit {
  public formGroup: FormGroup;
  public gridData: GridDataResult;
  public allGenres: Genre[];
  public allAuthors: Author[];
  private editedRowIndex: number;
  public pageSize = 9;
  public skip = 0;
  constructor(private service: BookService, http: HttpClient, @Inject('BASE_URL') baseUrl: string, private fb: FormBuilder) {
    this.service.getGenres().subscribe(result => {
      this.allGenres = result;
    }, error => console.error(error));

    this.service.getAuthors().subscribe(result => {
      this.allAuthors = result;
    }, error => console.error(error));
  
    this.loadItems();
  }

  public ngOnInit(): void {
  }

  public createForm() {
    debugger;
    this.formGroup = this.fb.group({
      'genre': {
        genreId: 0,
        genreName: ''
      },
      'bookName': '',
      'pages': 0,
      'publisher': '',
      'authors': new FormControl()
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.createForm();
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.formGroup = createFormGroup(dataItem);
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
    const book = formGroup.value;
    this.service.save(book, isNew);
    sender.closeRow(rowIndex);
    this.loadItems();
  }

  public removeHandler({ dataItem }): void {
    this.service.remove(dataItem);
    this.loadItems();
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
  }
  private loadItems(): void {
    this.service.getBooks().subscribe(result => {
      this.gridData = {
        data: result.data.slice(this.skip, this.skip + this.pageSize),
        total: result.data.length
      };
    }, error => console.error(error));
  }
  public log(data): void { console.log(data); }
}




