import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class BookService {
  private headers;
  private options;
  private http: any;
  private baseUrl: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
    this.baseUrl = baseUrl;
    this.http = http;
  }

  public getBooks(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Book/GetBooks');
  }
  public getGenres(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Book/GetAllGenres');
  }
  public getAuthors(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Book/GetAllAuthors');
  }
  public remove(book: any): void {
    return this.http.post(this.baseUrl + 'api/Book/Destroy', book, { headers: this.headers })
      .subscribe();
  }
  public updateBook(book: any): Observable<any> {
    return this.http.post(this.baseUrl + 'api/Book/Update', book, { headers: this.headers })
      .subscribe();
  }
  public createBook(book: any): Observable<any> {
    return this.http.post(this.baseUrl + 'api/Book/Create', book, { headers: this.headers })
      .subscribe();
  }
  public save(book: any, isNew: boolean): void {
    let body = JSON.stringify(book);
    if (isNew) {
      book.bookId = 0;
      this.createBook(book);
    } else {
      this.updateBook(book);
    }
  }
}
