import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class BrochureService {
  private headers;
  private options;
  private http: any;
  private baseUrl: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
    this.baseUrl = baseUrl;
    this.http = http;
  }

  public getBrochures(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Brochure/GetBrochures');
  }
  public getAuthors(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Brochure/GetAllAuthors');
  }
  public remove(brochure: any): void {
    return this.http.post(this.baseUrl + 'api/Brochure/Destroy', brochure, { headers: this.headers })
      .subscribe();
  }
  public updateBrochure(brochure: any): Observable<any> {
    debugger;
    return this.http.post(this.baseUrl + 'api/Brochure/Update', brochure, { headers: this.headers })
      .subscribe();
  }
  public createBrochure(brochure: any): Observable<any> {
    debugger;
    return this.http.post(this.baseUrl + 'api/Brochure/Create', brochure, { headers: this.headers })
      .subscribe();
  }
  public save(brochure: any, isNew: boolean): void {
    let body = JSON.stringify(brochure);
    if (isNew) {
      brochure.brochureId = 0;
      this.createBrochure(brochure);
    } else {
      this.updateBrochure(brochure);
    }
  }
}
