import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Author } from '../models/author';
import { Subscription } from '../models/subscription';
import { PublicationService } from './publication.service';
import { formatDate } from '@progress/kendo-angular-intl/dist/es/main';

const createFormGroup = dataItem => new FormGroup({
  'publicationId': new FormControl(dataItem.publicationId), 
  'publicationName': new FormControl(dataItem.publicationName, Validators.required),
  'pages': new FormControl(dataItem.pages),
  'publisher': new FormControl(dataItem.publisher),
  'subscription': new FormControl(dataItem.subscription, Validators.required),
  'publicationDate': new FormControl(new Date(dataItem.publicationDate), Validators.required),
  'authors': new FormControl(dataItem.authors)
});

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {
  public formGroup: FormGroup;
  public gridData: GridDataResult;
  public allSubscriptions: Subscription[];
  public allAuthors: Author[];
  private editedRowIndex: number;
  public pageSize = 9;
  public skip = 0;
  constructor(private service: PublicationService, http: HttpClient, @Inject('BASE_URL') baseUrl: string, private fb: FormBuilder) {
    this.service.getSubscriptions().subscribe(result => {
      this.allSubscriptions = result;
    }, error => console.error(error));

    this.service.getAuthors().subscribe(result => {
      this.allAuthors = result;
    }, error => console.error(error));

    this.loadItems();
  }

  public ngOnInit(): void {
  }

  public createForm() {
    this.formGroup = this.fb.group({
      'subscription': {
        subscriptionId: 0,
        subscriptionName: ''
      },
      'publicationName': '',
      'pages': 0,
      'publisher': '', 
      'publicationDate': new Date(),
      'authors': new FormControl()
    });
  }
  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.createForm();
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.formGroup = createFormGroup(dataItem);
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
    const publication = formGroup.value;
    this.service.save(publication, isNew);
    sender.closeRow(rowIndex);
    this.loadItems();
  }

  public removeHandler({ dataItem }): void {
    this.service.remove(dataItem);
    this.loadItems();
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
  }
  private loadItems(): void {
    this.service.getPublications().subscribe(result => {
      this.gridData = {
        data: result.data.slice(this.skip, this.skip + this.pageSize),
        total: result.data.length
      };
    }, error => console.error(error));
  }
  public log(data): void { console.log(data); }
}
