import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class PublicationService {
  private headers;
  private options;
  private http: any;
  private baseUrl: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
    this.baseUrl = baseUrl;
    this.http = http;
  }

  public getPublications(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Publication/GetPublications');
  }
  public getSubscriptions(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Publication/GetAllSubscriptions');
  }
  public getAuthors(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/Publication/GetAllAuthors');
  }
  public remove(publication: any): void {
    return this.http.post(this.baseUrl + 'api/Publication/Destroy', publication, { headers: this.headers })
      .subscribe();
  }
  public updatePublication(publication: any): Observable<any> {
    return this.http.post(this.baseUrl + 'api/Publication/Update', publication, { headers: this.headers })
      .subscribe();
  }
  public createPublication(publication: any): Observable<any> {
    return this.http.post(this.baseUrl + 'api/Publication/Create', publication, { headers: this.headers })
      .subscribe();
  }
  public save(publication: any, isNew: boolean): void {
    debugger;
    let body = JSON.stringify(publication);
    if (isNew) {
      publication.publicationId = 0;
      this.createPublication(publication);
    } else {
      this.updatePublication(publication);
    }
  }
}
