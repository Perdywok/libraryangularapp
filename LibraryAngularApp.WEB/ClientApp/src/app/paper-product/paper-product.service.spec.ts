import { TestBed, inject } from '@angular/core/testing';

import { PaperProductService } from './paper-product.service';

describe('PaperProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaperProductService]
    });
  });

  it('should be created', inject([PaperProductService], (service: PaperProductService) => {
    expect(service).toBeTruthy();
  }));
});
