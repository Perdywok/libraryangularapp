import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class PaperProductService {
  private headers;
  private options;
  private http: any;
  private baseUrl: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
    this.baseUrl = baseUrl;
    this.http = http;
  }

  public getPaperProducts(): Observable<any> {
    return this.http.get(this.baseUrl + 'api/PaperProduct/GetPaperProducts');
  }
}
