import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Author } from '../models/author';
import { Subscription } from '../models/subscription';
import { PaperProductService } from './paper-product.service';

@Component({
  selector: 'app-paper-product',
  templateUrl: './paper-product.component.html',
  styleUrls: ['./paper-product.component.css']
})
export class PaperProductComponent implements OnInit {
  public gridData: GridDataResult;
  public pageSize = 9;
  public skip = 0;
  constructor(private service: PaperProductService, http: HttpClient, @Inject('BASE_URL') baseUrl: string, private fb: FormBuilder) {
    this.loadItems();
  }

  public ngOnInit(): void {
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
  }

  private loadItems(): void {
    this.service.getPaperProducts().subscribe(result => {
      this.gridData = {
        data: result.data.slice(this.skip, this.skip + this.pageSize),
        total: result.data.length
      };
    }, error => console.error(error));
  }
  public log(data): void { console.log(data); }
}

