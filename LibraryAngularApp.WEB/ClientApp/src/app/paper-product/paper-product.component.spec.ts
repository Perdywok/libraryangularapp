import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperProductComponent } from './paper-product.component';

describe('PaperProductComponent', () => {
  let component: PaperProductComponent;
  let fixture: ComponentFixture<PaperProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaperProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaperProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
