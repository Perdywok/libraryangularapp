import { Genre } from "./genre";
import { Author } from "./author";

export class Book {
  public bookId: number;
  public bookName = '';
  public pages: number;
  public publisher = '';
  public genre: Genre;
  public authors: Author[];
}
