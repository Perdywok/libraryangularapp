"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Publication = /** @class */ (function () {
    function Publication() {
        this.publicationName = '';
        this.publisher = '';
        this.type = '';
    }
    return Publication;
}());
exports.Publication = Publication;
//# sourceMappingURL=publication.js.map