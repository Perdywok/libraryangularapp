import { Author } from "./author";

export class Brochure {
  public paperProductId: number;
  public paperProductName = '';
  public pages: number;
  public publisher = '';
  public authors: Author[];
}
