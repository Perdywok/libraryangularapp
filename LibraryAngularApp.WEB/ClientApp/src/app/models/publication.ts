import { Subscription } from "rxjs/Subscription";
import { Author } from "./author";

export class Publication {
  public publicationId: number;
  public publicationName = '';
  public pages: number;
  public publisher = '';
  public subscription: Subscription;
  public type = '';
  public publicationDate: Date;
  public authors: Author[];
}
