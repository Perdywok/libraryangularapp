import { Author } from "./author";

export class Brochure {
  public brochureId: number;
  public bookName = '';
  public pages: number;
  public publisher = '';
  public authors: Author[];
}
