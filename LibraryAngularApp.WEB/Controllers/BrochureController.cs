﻿using System.Collections.Generic;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LibraryAngularApp.BLL.Services;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL;
using Microsoft.AspNetCore.Mvc;

namespace LibraryAngularApp.WEB.Controllers
{
    [Route("api/[controller]")]
    public class BrochureController : Controller
    {
        private BrochureService brochureService;

        public BrochureController(Library context)
        {
            brochureService = new BrochureService(context);
        }

        [HttpGet("[action]")]
        public ActionResult GetBrochures([DataSourceRequest]DataSourceRequest request)
        {
            var result = Json(brochureService.GetAllObjects().ToDataSourceResult(request));
            return result;
        }

        [HttpPost("[action]")]
        public ActionResult Update([FromBody] BrochureViewModel brochure)
        {
            if (ModelState.IsValid)
            {
                brochureService.UpdateObject(brochure);
            }

            return Json(new[] { brochure }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost("[action]")]
        public ActionResult Create([FromBody] BrochureViewModel brochure)
        {

            if (ModelState.IsValid)
            {
                brochure.BrochureId = brochureService.CreateObject(brochure);
            }
            return Json(new[] { brochure }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost("[action]")]
        public ActionResult Destroy([FromBody] BrochureViewModel brochure)
        {
            if (ModelState.IsValid)
            {
                brochureService.DeleteObject(brochure);
            }
            return Json(new[] { brochure }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpGet("[action]")]
        public ActionResult GetAllAuthors()
        {
            List<AuthorViewModel> authors = brochureService.GetAuthors();
            return Json(authors);
        }

    }
}