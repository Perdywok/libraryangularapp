﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LibraryAngularApp.BLL.Services;
using LibraryAngularApp.DAL;
using Microsoft.AspNetCore.Mvc;

namespace LibraryAngularApp.WEB.Controllers
{
    [Route("api/[controller]")]
    public class PaperProductController : Controller
    {
        private PaperProductService publicationService;

        public PaperProductController(Library context)
        {
            publicationService = new PaperProductService(context);
        }

        [HttpGet("[action]")]
        public ActionResult GetPaperProducts([DataSourceRequest]DataSourceRequest request)
        {
            var result = Json(publicationService.GetAllObjects().ToDataSourceResult(request));
            return result;
        }       
    }
}