﻿using System.Collections.Generic;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LibraryAngularApp.BLL.Services;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL;
using Microsoft.AspNetCore.Mvc;

namespace LibraryAngularApp.WEB.Controllers
{
    [Route("api/[controller]")]
    public class BookController : Controller
    {
        private BookService bookService;

        public BookController(Library context)
        {
            bookService = new BookService(context);
        }

        [HttpGet("[action]")]
        public ActionResult GetBooks([DataSourceRequest]DataSourceRequest request)
        {
            var result = Json(bookService.GetAllObjects().ToDataSourceResult(request));
            return result;
        }

        [HttpPost("[action]")]
        public ActionResult Update([FromBody] BookViewModel book)
        {
            if (ModelState.IsValid)
            {
                bookService.UpdateObject(book);
            }

            return Json(new[] { book }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost("[action]")]
        public ActionResult Create([FromBody] BookViewModel book)
        {

            if (ModelState.IsValid)
            {
                book.BookId = bookService.CreateObject(book);
            }
            return Json(new[] { book }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost("[action]")]
        public ActionResult Destroy([FromBody] BookViewModel book)
        {
            if (ModelState.IsValid)
            {
                bookService.DeleteObject(book);
            }
            return Json(new[] { book }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpGet("[action]")]
        public ActionResult GetAllAuthors()
        {
            List<AuthorViewModel> authors = bookService.GetAuthors();
            return Json(authors);
        }

        [HttpGet("[action]")]
        public ActionResult GetAllGenres()
        {
            List<GenreViewModel> genres = bookService.GetAllGenres();
            return Json(genres);
        }
    }
}