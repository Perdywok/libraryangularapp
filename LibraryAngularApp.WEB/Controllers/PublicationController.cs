﻿using System.Collections.Generic;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LibraryAngularApp.BLL.Services;
using LibraryAngularApp.BLL.ViewModels;
using LibraryAngularApp.DAL;
using Microsoft.AspNetCore.Mvc;

namespace LibraryAngularApp.WEB.Controllers
{
    [Route("api/[controller]")]
    public class PublicationController : Controller
    {
        private PublicationService publicationService;

        public PublicationController(Library context)
        {
            publicationService = new PublicationService(context);
        }

        [HttpGet("[action]")]
        public ActionResult GetPublications([DataSourceRequest]DataSourceRequest request)
        {
            var result = Json(publicationService.GetAllObjects().ToDataSourceResult(request));
            return result;
        }

        [HttpPost("[action]")]
        public ActionResult Update([FromBody] PublicationViewModel publication)
        {
            if (ModelState.IsValid)
            {
                publicationService.UpdateObject(publication);
            }

            return Json(new[] { publication }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost("[action]")]
        public ActionResult Create([FromBody]PublicationViewModel publication)
        {

            if (ModelState.IsValid)
            {
                publication.PublicationId = publicationService.CreateObject(publication);
            }
            return Json(new[] { publication }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost("[action]")]
        public ActionResult Destroy([FromBody] PublicationViewModel publication)
        {
            if (ModelState.IsValid)
            {
                publicationService.DeleteObject(publication);
            }
            return Json(new[] { publication }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpGet("[action]")]
        public ActionResult GetAllAuthors()
        {
            List<AuthorViewModel> authors = publicationService.GetAuthors();
            return Json(authors);
        }

        [HttpGet("[action]")]
        public ActionResult GetAllSubscriptions()
        {
            List<SubscriptionViewModel> subsriptions = publicationService.GetAllSubscriptions();
            return Json(subsriptions);
        }
    }
}